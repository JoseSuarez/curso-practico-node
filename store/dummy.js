const db = {
    'user': [
        {id: '1', username: 'Leo'},
    ],
};

async function list(table) {
    return db[table] || [];
};

async function get(table, id) {
    let coll = await list(table);
    return coll.filter(item => item.id === id)[0] || null;
};

async function upsert(table, data) {
    if (!db[table]) {
        db[table] = [];
    }

    db[table].push(data);

    console.log(db);
};

async function remove(table, id) {
    return true;
};

async function query(table, q) {    
    let coll = await list(table);
    let keys = Object.keys(q);
    let key = keys[0];
    return coll.filter(item => item[key] === q[key])[0] || null;
}

module.exports = {
    list, get, upsert, remove, query
}